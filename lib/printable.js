import esDirname from 'es-dirname'
import { promises as fs } from 'fs'
import handlebars from 'handlebars'
import path from 'path'

const __dirname = esDirname()

function wrapEnvBlock(env, node) {
  const text = handleBlock(node)
  return `\\begin{${env}}\n${text}\n\\end{${env}}`
}

function handleNode(node) {
  switch (node.type) {
    case 'blockquote':
      return wrapEnvBlock('quote', node)
    case 'definition':
      // TODO: Consume definitions.
      console.error('definition ignored: ${node.label} ${node.url}')
      return null
    case 'emphasis':
      return `\\textit{${handleInlineBlock(node)}}`
    case 'heading':
      return handleHeading(node)
    case 'html':
      // TODO: Implement percent comment.
      console.error('node type "html" not handled')
      return null
    case 'image':
      return handleImage(node)
    case 'linkReference':
      return `\\url{${handleInlineBlock(node)}}`
    case 'list':
      return handleList(node)
    case 'paragraph':
      return handleInlineBlock(node)
    case 'strong':
      return `\\textbf{${handleInlineBlock(node)}}`
    case 'text':
      return handleText(node)
  }
  console.error(`unhandled type: ${node.type}`)
  return null
}

function handleHeading(node) {
  const text = handleInlineBlock(node)
  switch(node.depth) {
    case 1:
      return `\\chapter{${text}}`
    case 2:
      return `\\subsection*{${text}}`
  }
  console.error(`unhandled heading depth: ${node.depth}`)
  return null
}

function handleImage(node) {
  const segments = ['\\begin{figure}']
  segments.push(`  \\includegraphics[width=\\textwidth]{${node.url}}`)
  segments.push(`  \\caption{${node.alt}}`)
  segments.push('\\end{figure}')
  return segments.join('\n')
}

function handleBlock(node) {
  const segments = []
  for (const c of node.children) {
    const s = handleNode(c)
    if (s) segments.push(s)
  }
  return segments.join('\n\n')
}

function handleInlineBlock(node) {
  const segments = []
  for (const c of node.children) {
    const s = handleNode(c)
    if (s) segments.push(s)
  }
  return segments.join('')
}

function handleList(node) {
  const segments = ['\\begin{enumerate}']
  for (const c of node.children) {
    switch (c.type) {
      case 'listItem':
        segments.push(`  \\item{${handleBlock(c)}}`)
        break
      default:
        console.error(`List node type unrecognized: ${c.type}`)
    }
  }
  segments.push('\\end{enumerate}')
  return segments.join('\n')
}

function handleText(node) {
  return node.value
    .replace(/([%$])/, '\\$1')  // Handle special characters
    .replace(/\^/, 'carrot')  // TODO: Fix this
}

async function writeMain(chapters) {
  const templatePath =
    path.join(__dirname, 'templates', 'printable.main.tex.handlebars')
  const templateText = await fs.readFile(templatePath, 'utf8')
  const template = handlebars.compile(templateText)
  const data = template({
    chapters
  })
  await fs.writeFile('main.tex', data)
}

async function runLatex() {
  // TODO
}

export default ({ buildDir }, opts) => {
  return {
    chapterExt: 'tex',
    remarkChapterPlugin(opts) {
      this.Compiler = compiler
    
      function compiler (root, file) {
        return handleBlock(root)
      }
    },
    async buildAfterChapters(buildDir, chapters) {
      const cwd = process.cwd()
      process.chdir(buildDir)
      await writeMain(chapters)
      process.chdir(cwd)
      await runLatex()
    }
  }
}
