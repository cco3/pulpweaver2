import { promises as fs } from 'fs'
import glob from 'globby'
import path from 'path'
import remark from 'remark'
import remarkGuide from 'remark-preset-lint-markdown-style-guide'
import resolveCwd from 'resolve-cwd'
import vfile from 'to-vfile'

import printableOutmod from './printable.js'

const isString = (s) => typeof s === 'string' || s instanceof String

const OUTMODS = {
  'printable': printableOutmod
}

async function getChapters({ chapters }) {
  chapters = chapters || []
  const cs = []
  for (const c of chapters) {
    const newCs = await glob(c)
    cs.push(...newCs)
  }
  return cs
}

async function loadConfig() {
  const configPath = path.resolve('pulpweaver.config.js')
  let mod
  try {
    mod = await import(configPath)
  } catch (e) {
    if (e.code === 'ERR_MODULE_NOT_FOUND') {
      console.error(e.message)
      return null
    }
    throw e
  }
  if (!mod.default) {
    console.error(`No default export found in ${configPath}.`)
    return null
  }

  return mod.default
}

async function getRemark(config) {
  let chain = remark().use(remarkGuide)

  const rconfig = config.remark || {}
  const plugins = rconfig.plugins || []

  for (const spec of plugins) {
    let mod = spec
    let opts = undefined
    if (Array.isArray(spec)) {
      mod = spec[0]
      opts = spec[1]
    }

    if (isString(mod)) {
      const loc = resolveCwd(spec)
      const { default: d } = await import(loc)
      mod = d
    }
    chain = chain.use(mod, opts)
  }
  return chain
}

async function buildForOutput(outConfig, config) {
  config = Object.assign({
    buildDir: 'build'
  }, config)
  outConfig = Object.assign({
    buildDir: outConfig.name
  }, outConfig)

  if (!outConfig.type) {
    console.error(`Output needs a type: ${JSON.stringify(outConfig, null, 2)}`)
    return
  }
  if (!outConfig.name) {
    console.error(`Output needs a name: ${JSON.stringify(outConfig, null, 2)}`)
    return
  }

  let outmod
  if (isString(outConfig.type)) {
    outmod = OUTMODS[outConfig.type]
    if (!outmod) {
      console.error(`Could not find module ${outConfig.type}`)
      return
    }
  }

  const buildDir = path.join(config.buildDir, outConfig.buildDir)
  const resolvedOutConfig = outmod({ buildDir }, outConfig.opts)

  const chapters = await getChapters(config)
  for (const c of chapters) {
    const f = await vfile.read(c)
    const chain = await getRemark(config)
    const res = await chain
      .use(resolvedOutConfig.remarkChapterPlugin)
      .process(f)
    const ext = resolvedOutConfig.chapterExt
    const chapterPath = path.join(buildDir, 'chapters', `${f.path}.${ext}`)
    try {
      await fs.mkdir(path.dirname(chapterPath))
    } catch (e) {
      if (e.code !== 'EEXIST') throw e
    }
    await fs.writeFile(chapterPath, res.contents)
    // TODO: print guide messages from f?
  }

  await resolvedOutConfig.buildAfterChapters(buildDir, chapters, outConfig)
}

async function build(dir='.') {
  const cwd = process.cwd()
  process.chdir(dir)
  const config = await loadConfig()
  if (!config) return false

  const output = config.output || []
  const outputNames = {}
  for (const o of output) {
    if (o.name in outputNames) {
      console.error(`Duplicate output name: ${o.name}`)
      continue
    }
    outputNames[o.name] = true
    await buildForOutput(o, config)
  }

  process.chdir(cwd)
  return true
}

export default build
