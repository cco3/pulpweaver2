#!/usr/bin/env node
import optimist from 'optimist'

import build from '../lib/build.js'

const SUBCMDS =  {
  'build': {
    fn: buildMain,
    opt: optimist.usage('Usage: $0 build <dir?>'),
    desc: 'Build the project'
  }
}

function oc(f, msg) {
  f.toString = () => msg
  return f
}

async function buildMain({_: [dir]}) {
  dir = dir || '.'
  const res = await build(dir)
  return res ? 0 : 1
}

async function main() {
  const help = Object.keys(SUBCMDS)
    .sort()
    .map(k => `${k} ${SUBCMDS[k].desc}`)
    .join('\n')
  const o = optimist.usage(`Usage: $0 <subcommand>\n${help}`)
    .check(oc(({_}) => !!_.length, 'must provide a subcommand'))
    .check(oc(({_: [s]}) => !!SUBCMDS[s], 'subcommand not recognized'))
  const s = SUBCMDS[o.argv._[0]]
  const argv = s.opt.argv
  argv._.shift() // Remove the subcommand
  return await s.fn(argv)
}

(async () => {
  let ret = -1
  try {
    ret = await main()
  } catch (e) {
    console.error(e)
    console.error(e.stack)
  }
  process.exit(ret)
})()
